// *************************************************************************
// Load more button on blogpage
jQuery(function($){
	$('.blog-loadmore').click(function(){
		var button = $(this),
			data = {
			'action': 'loadmore',
			'query': vm_loadmore_params.posts, // that's how we get params from wp_localize_script() function
			'page' : vm_loadmore_params.current_page
		};

		$.ajax({
			url : vm_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Laden...'); // change the button text, you can also add a preloader image
			},
			success : function( data ){
				if( data ) {
					button.text( 'Meer Laden' ); // insert new posts
					$('.s-news>div>.items').append(data);
					vm_loadmore_params.current_page++;

					if ( vm_loadmore_params.current_page == vm_loadmore_params.max_page )
						button.remove(); // if last page, remove the button

					// you can also fire the "post-load" event here if you use a plugin that requires it
					// $( document.body ).trigger( 'post-load' );
				} else {
					button.remove(); // if no data, remove the button as well
				}
			}
		});
	});
});
