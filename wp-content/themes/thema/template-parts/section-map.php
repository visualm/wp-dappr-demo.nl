
<?php if (get_field('longitude', 'option') && get_field('latitude', 'option')): ?>

	<section class="s-maps">
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.3/dist/leaflet.css" integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ==" crossorigin=""/>
		<script src="https://unpkg.com/leaflet@1.3.3/dist/leaflet.js" integrity="sha512-tAGcCfR4Sc5ZP5ZoVz0quoZDYX5aCtEm/eu1KhSLj2c9eFrylXZknQYmxUssFaVJKvvc0dJQixhGjG2yXWiV9Q==" crossorigin=""></script>

		<div id="map"></div>

		<?php
			$lng = get_field('longitude', 'option');
			$lat = get_field('latitude', 'option');
		?>

		<script>
			var mymap = L.map('map', {
				scrollWheelZoom: false,
			}).setView([<?=$lng;?>, <?=$lat;?>], 13);

			scrollWheelZoom: false
			L.tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}{r}.png', {foo: 'bar'}).addTo(mymap);
			var marker = L.marker([<?=$lng;?>, <?=$lat;?>]).addTo(mymap);

		</script>
	</section>
<?php endif; ?>
