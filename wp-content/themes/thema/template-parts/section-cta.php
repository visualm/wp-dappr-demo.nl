<?php $cta_bg = get_field( 'cta_bg', 'option' ); ?>

<?php if (get_field( 'cta_bg', 'option' ) || get_field( 'cta_title', 'option' )): ?>

	<section class="s-cta">

		<?php if ($cta_bg): ?>
			<div class="background lazy lazy-bg" data-bg="url('<?=wp_get_attachment_url($cta_bg, 'full');?>')"></div>
		<?php else: ?>
				<div class="background"></div>
		<?php endif; ?>

		<div class="overlay"></div>

		<div class="container">
			<?php if (get_field( 'cta_title', 'option' )): ?>
				<h2><?php the_field( 'cta_title', 'option' ); ?></h2>
			<?php endif; ?>

			<?php if (get_field( 'cta_content', 'option' )): ?>
				<p><?php the_field( 'cta_content', 'option' ); ?></p>
			<?php endif; ?>

			<?php if (get_field('cta_btn_url', 'option')): ?>
				<div class="buttons">
					<a href="<?php the_field( 'cta_btn_url', 'option' ); ?>" class="button--light has-icon"><?php the_field( 'cta_btn_text', 'option' ); ?><i class="fas fa-chevron-right"></i></a>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
