<?php
/**
  * Auto Version autoVer('styles.css') By Dennis Prins
  */
	function autoVer($filename){
		$url = get_template_directory_uri() . $filename;
		$file = get_template_directory() . $filename;
		if ( file_exists($file)) {
		    return $url . '?v=' .md5(date("FLdHis", filectime($file)) . filesize($file));
		}
		clearstatcache();
	}
?>
