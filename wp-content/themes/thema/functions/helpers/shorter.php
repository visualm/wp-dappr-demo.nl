<?php
//******************************************************************************
/* Shorter -> <?=shorter(get_the_excerpt(), 100);?> */
function shorter($content, $size) {
  if ($content && $size):
    if(strlen($content) > $size) {
      return substr($content,0,$size) . "...";
    } else { return $content;}
  endif;
}
?>
