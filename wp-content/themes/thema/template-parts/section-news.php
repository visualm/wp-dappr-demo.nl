<?php
	$args = array(
		'post_type' 		=> 'post',
		'posts_per_page' 	=> '3',
		'post_status'		=> 'publish'
	);

	$news = new WP_Query( $args );
?>

<?php if ( $news->have_posts() ) : ?>
	<section class="s-news">
		<div class="container">
			<h2><?=__('Laatste nieuws', 'Homepage');?></h2>
			<div class="items">
				<?php while ( $news->have_posts() ) : $news->the_post(); ?>
					<a class="item" href="<?=the_permalink();?>">
						<div class="image">
							<?php if (get_the_post_thumbnail_url()): ?>
								<img class="lazy" data-src="<?=get_the_post_thumbnail_url($post->ID ,'news');?>" alt="Foto - <?=the_title();?>">
							<?php else:?>
								<img class="lazy" data-src="<?=getPlaceholderIMG('390', '250')?>" alt="Foto - <?=the_title();?>">
							<?php endif; ?>
						</div>

						<div class="content">
							<strong><?=the_title();?></strong>
							<p><?=the_excerpt();?></p>
						</div>
					</a>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
