<div class="sharebox">
	<span class="title"><?=__('Deel deze pagina', 'visualmedia');?></span>
	<ul class="social-icons">
		<li class="share"><a data-share="facebook" data-title="Bekijk deze link" data-link="<?=the_permalink();?>" data-description="<?=wp_title();?>" href="https://www.facebook.com/sharer/sharer.php?u=<?=the_permalink();?>" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a></li>
		<li class="share"><a data-share="twitter" data-title="Bekijk deze link" data-link="<?=the_permalink();?>" data-hashtags="<?=wp_title();?>" href="https://www.twitter.com/share?url=<?=the_permalink();?>" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
		<li class="share"><a data-share="linkedin" data-title="Bekijk deze link" data-link="<?=the_permalink();?>" data-summery="<?=wp_title();?>" href="https://www.linkedin.com/shareArticle?mini=true&url=<?=the_permalink();?><" target="_blank" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
		<li class="share"><a href="mailto:?subject=Bekijk deze link&body=<?=the_permalink();?>" class="mail"><i class="fa fa-envelope"></i></a></li>
	</ul>
</div>
