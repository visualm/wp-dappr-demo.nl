<?php // default sidebar ?>

 <aside class="sidebar">

	 <?php if (get_the_post_thumbnail()): ?>
	 	<div class="image">
			<img class="lazy" data-src="<?=get_the_post_thumbnail_url($post->ID ,'sidebar');?>" alt="sidebar">
		</div>
	 <?php endif; ?>

	 <?php if (get_field( 'sidebar_content' )): ?>
		 <div class="content">
    	 	<h3>Dit is een titel</h3>
			<p><?=get_field( 'sidebar_content' );?></p>
			<?php if (get_field( 'sidebar_content_btn_url')): ?>
 			   <a href="<?=get_field( 'sidebar_content_btn_url');?>" class="button--light has-icon"><?=get_field( 'sidebar_content_btn_tekst'); ?><i class="fas fa-chevron-right"></i></a>
 		   <?php endif; ?>
    	 </div>
	<?php endif; ?>

	<?php if (get_field( 'sidebar_youtube_id' )): ?>
		<?php $youtube_id = get_field( 'sidebar_youtube_id' ); ?>
		 <a class="video lazy" href="https://www.youtube.com/watch?v=<?=$youtube_id;?>" data-lity style="background-image:url('http://i.ytimg.com/vi/<?=$youtube_id;?>/maxresdefault.jpg')">
			 <div class="overlay">
				 <i class="far fa-play-circle"></i>
			 </div>
		 </a>
	<?php endif; ?>
 </aside>
