<section class="s-404">
	<div class="container">
		<h1>Pagina niet gevonden</h1>
		<p>
		  Helaas, de opgegeven pagina kon niet worden gevonden. <br />
		  Klik <a href="<?=get_bloginfo('url');?>" title="home">hier</a> om terug te keren naar de hoofdpagina.
		</p>

	</div>
</section>
