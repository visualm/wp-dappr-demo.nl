<?php
	if (get_field('hero_bg')):
		$heroImgId = get_field('hero_bg');
	else:
		$heroImgId = get_field('hero_fallback', 'options');
	endif;


	if (get_field('hero_title')) :
		$title = get_field('hero_title');
	elseif(is_404()) :
		$title = __('Pagina niet gevonden', 'werkexpert');
	else :
		$title = get_the_title();
	endif;

	if (is_singular('post')) :
		if (get_field('hero_subtitle')) :
			$subtitle = get_field('hero_subtitle');
		else :
			$subtitle = get_the_date('d F Y', get_the_ID());
		endif;
	else :
		if (get_field('hero_subtitle')) :
			$subtitle = get_field('hero_subtitle');
		else :
			$subtitle = "";
		endif;
	endif;
?>

<section class="s-hero lazy lazy-bg" data-bg="url(<?=wp_get_attachment_image_url($heroImgId, 'full'); ?>)">
	<div class="overlay"></div>
	<div class="container">

		<?php if ($title): ?>
			<h1><?=$title?></h1>
		<?php endif; ?>

		<?php if ($subtitle): ?>
			<span class="hero__subtitle"><?=$subtitle;?></span>
		<?php endif; ?>

	</div>
</section>
