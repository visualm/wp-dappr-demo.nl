<?php

	/* Template Name: Blog overzicht */

	get_header();

	get_template_part('template-parts/section', 'builder');
	get_template_part('template-parts/section', 'newsfull');
	get_template_part('template-parts/section', 'cta');
	get_template_part('template-parts/section', 'logoslider');

	get_footer();
