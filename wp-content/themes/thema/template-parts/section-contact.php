<section class="s-contact">
	<div class="container">
		<div class="contact-form">
			<?php get_template_part('template-parts/partials/builder', 'full');?>
			<?php get_template_part('template-parts/forms/form', 'contact');?>
		</div>

		<div class="contact-info">
			<div class="block info">
				<h3><?=__('Adresgegevens', 'Contact');?></h3>
				<?php if (get_field( 'name', 'option' )): ?>
					<span><strong class="title"><?php the_field( 'name', 'option' ); ?></strong></span>
				<?php endif; ?>

				<?php if (get_field( 'adres', 'option' )): ?>
					<span><?php the_field( 'adres', 'option' ); ?></span>
				<?php endif; ?>

				<?php if (get_field( 'zipcode', 'option' ) || get_field( 'city', 'option')): ?>
					<span><?php the_field( 'zipcode', 'option' ); ?> <?php the_field( 'city', 'option' ); ?></span>
				<?php endif; ?>
				<br>
				<br>
				<?php if (get_field( 'phone', 'option' )): ?>
					<span><a href="tel:<?=get_field('phone', 'options');?>"><i class="fas fa-phone"></i><?=get_field('phone', 'options');?></a></span>
				<?php endif; ?>

				<?php if (get_field( 'email', 'option' )): ?>
					<span><a href="mailto<?=antispambot(get_field( 'email', 'option' ));?>"><i class="fas fa-envelope"></i> <?=antispambot(get_field( 'email', 'option' ));?></a></span>
				<?php endif; ?>

				<?php if (get_field( 'email_support', 'option' )): ?>
					<span><a href="mailto<?=antispambot(get_field( 'email_support', 'option' ));?>"><i class="fas fa-headset"></i> <?=antispambot(get_field( 'email_support', 'option' ));?></a></span>
				<?php endif; ?>
				<br>
				<br>
				<?php if (get_field( 'kvk', 'option' )): ?>
					<span><strong>KvK:</strong> 	<?php the_field( 'kvk', 'option' ); ?></span>
				<?php endif; ?>

				<?php if (get_field( 'btw', 'option' )): ?>
					<span><strong>BTW:</strong> 	<?php the_field( 'btw', 'option' ); ?></span>
				<?php endif; ?>

				<?php if (get_field( 'iban', 'option' )): ?>
					<span><strong>iBAN:</strong> 	<?php the_field( 'iban', 'option' ); ?></span>
				<?php endif; ?>
			</div>

			<?php if (get_field( 'route_url', 'option' )): ?>
				<?php
				if (get_the_post_thumbnail_url()) {
					$contact_img = get_the_post_thumbnail_url();
				} else {
					$contact_img = GetPlaceholderIMG(370, 320);
				} ?>


				<div class="block route lazy lazy-bg" data-bg="url(<?=$contact_img;?>)">
					<a href="<?php the_field( 'route_url', 'option' ); ?>" class="button"><i class="fa fa-map-marker" aria-hidden="true"></i><?=__('Plan route', 'Contact');?></a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
