<section class="s-content">
	<div class="container">
		<div class="content">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
				<h1><?=the_title();?></h1>
				<?php the_content(); ?>
				<?php get_template_part('template-parts/partials/social', 'share');?>
			<?php endwhile; endif; ?>
		</div>
		<?=get_sidebar();?>
	</div>
</section>
