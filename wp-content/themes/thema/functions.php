<?php
require_once (TEMPLATEPATH . '/functions/head.php'); // declare output related to the head
require_once (TEMPLATEPATH . '/functions/init.php'); // initiates stuff for theme
require_once (TEMPLATEPATH . '/functions/theme-secure.php'); // wordpress/theme security
require_once (TEMPLATEPATH . '/functions/theme-support.php');// declare output related to theme options
require_once (TEMPLATEPATH . '/functions/helpers.php'); // add custom helpers
require_once (TEMPLATEPATH . '/functions/filters.php'); // declare content filters
require_once (TEMPLATEPATH . '/functions/custom-fields.php'); // declare content filters
require_once (TEMPLATEPATH . '/functions/admin.php'); // theme options /admin
?>
