<?php

	/* Template Name: Contact */

	get_header();

	get_template_part('template-parts/section', 'hero');
	get_template_part('template-parts/section', 'breadcrumb');
	get_template_part('template-parts/section', 'contact');
	get_template_part('template-parts/section', 'map');
	get_template_part('template-parts/section', 'logoslider');

	get_footer();
