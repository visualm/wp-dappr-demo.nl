<?php $class = "";?>
<?php
if (is_page_template('template-contact.php')):
	$class = "contact";
elseif(is_page_template('archive-blog.php')):
	$class = "contact";
endif;
?>

<?php if ( have_rows( 'logos', 'option' ) ) : ?>
	<section class="s-logoslider <?=$class;?>">
		<div id="slider-logo">
			<?php while ( have_rows( 'logos', 'option' ) ) : the_row(); ?>
				<?php $logo = get_sub_field( 'logo' ); ?>
				<div class="slide">
					<img src="<?=wp_get_attachment_url( $logo, 'logo' );?>" alt="Logo">
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>
