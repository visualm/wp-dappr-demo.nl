	<?php $phone = preg_replace('/\s+/', '', get_field("phone","options")); ?>

	<footer class="footer">
		<div class="container">
			<div class="column">
				<h3><?php echo esc_html_e( 'Contactgegevens', 'Footer' );?></h3>
				<?php if (get_field('name', 'options')){ echo get_field('name', 'options'); }?><br>
				<?php if (get_field('adres', 'options')){ echo get_field('adres', 'options'); }?><br>
				<?php if (get_field('zipcode', 'options')){ echo get_field('zipcode', 'options'); }?> <?php if (get_field('city', 'options')){ echo get_field('city', 'options'); }?><br>
				<br>
				<?php if (get_field('phone', 'options')): ?>
					<a href="tel:<?=$phone;?>"></a><i class="fas fa-phone"></i> <?=get_field('phone', 'options');?></a><br>
				<?php endif; ?>

				<?php if (get_field('phone', 'options')): ?>
					<a href="mailto:<?=antispambot(get_field('email', 'options'));?>"></a><i class="fas fa-envelope"></i> <?=antispambot(get_field('email', 'options'));?></a>
				<?php endif; ?>
			</div>

			<div class="column text">
				<h3><?php the_field( 'footer_titel', 'option' ); ?></h3>
				<p><?php the_field( 'footer_text', 'option' ); ?></p>
			</div>

			<div class="column social">
				<h3><?php echo esc_html_e( 'Volg ons op social media', 'Footer' );?></h3>
				<div class="socials">

				<?php if (get_field('linkedin', 'options')): ?>
					<a href="<?=get_field('linkedin', 'options');?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
				<?php endif; ?>

				<?php if (get_field('facebook', 'options')): ?>
					<a href="<?=get_field('facebook', 'options');?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
				<?php endif; ?>

				<?php if (get_field('twitter', 'options')): ?>
					<a href="<?=get_field('twitter', 'options');?>" target="_blank"><i class="fab fa-twitter"></i></a>
				<?php endif; ?>

				<?php if (get_field('googleplus', 'options')): ?>
					<a href="<?=get_field('googleplus', 'options');?>" target="_blank"><i class="fab fa-google-plus-g"></i></a>
				<?php endif; ?>

				<?php if (get_field('instagram', 'options')): ?>
					<a href="<?=get_field('instagram', 'options');?>" target="_blank"><i class="fab fa-instagram"></i></a>
				<?php endif; ?>

				<?php if (get_field('youtube', 'options')): ?>
					<a href="<?=get_field('youtube', 'options');?>" target="_blank"><i class="fab fa-youtube"></i></a>
				<?php endif; ?>
			</div>
			</div>
		</div>
	</footer>

	<div id="copyright">
		<div class="container">
			<div class="column">
				<a href="<?=get_bloginfo('url');?>"><footer>Copyright &copy; <?php echo date('Y'); ?> <?=get_bloginfo('name');?> </footer></a>
			</div>

			<div class="column">
				<nav class="nav-footer">
					<?php wp_nav_menu( array(
						'theme_location' => 'footer-menu',
						'depth' => '1',
					)); ?>
				</nav>
			</div>
		</div>
	</div>

    <!-- scripts -->
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "LocalBusiness",
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "<?=get_field("address","options");?>",
        "addressLocality": "<?=get_field("zipcode","options");?>",
        "addressRegion": "<?=get_field("city","options");?>"
      },
      "name": "<?=get_bloginfo('name');?>","telephone": "<?=$phone;?>",
      "email": "<?=get_field("email","options");?>"
    }
    </script>
    <script>
      var themeURL = '<?=get_template_directory_uri(); ?>';
    </script>
    <script type="text/javascript">
    [
      'https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js',
	  '<?=autoVer('/dist/js/loadmore.min.js');?>',
	  '<?=autoVer('/dist/js/slick.min.js');?>',
	  '<?=autoVer('/dist/js/lity.min.js');?>',
	  '<?=autoVer('/dist/js/lazyload.min.js');?>',
      '<?=autoVer('/dist/js/main.min.js');?>',
      ].forEach(function(src) {
      var script = document.createElement('script');
      script.src = src;
      script.async = false;
      document.body.appendChild(script);
    });
    </script>

    <?php wp_footer();?>
	</body>
</html>
