<a class="item" href="<?=the_permalink();?>">
	<div class="image">
		<?php if (get_the_post_thumbnail_url()): ?>
			<img class="lazy" data-src="<?=get_the_post_thumbnail_url($post->ID ,'news');?>" alt="Foto - <?=the_title();?>">
		<?php else:?>
			<img class="lazy" data-src="<?=getPlaceholderIMG('390', '250')?>" alt="Foto - <?=the_title();?>">
		<?php endif; ?>
	</div>

	<div class="content">
		<strong><?=the_title();?></strong>
		<p><?=the_excerpt();?></p>
	</div>
</a>
