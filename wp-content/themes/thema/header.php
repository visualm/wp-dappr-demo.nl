<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
	<title><?php wp_title(''); ?></title>
	<!-- Meta -->
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="author" content="<?=get_bloginfo('name');?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Link -->
	<style>
		<?= include 'dist/css/style.critical.css';?>
	</style>
	<link rel="stylesheet" href="<?=autoVer('/dist/css/style.css');?>">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">

	<!-- Scripts -->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<section class="s-topbar">
		<div class="container">
			<?php if (get_field('phone','options')): ?>
				<div class="item">
					<?php $phone = preg_replace('/\s+/', '', get_field('phone','options')); ?>
					<i class="fas fa-phone"></i> <a href="tel:<?=$phone;?>"><?=get_field('phone', 'option');?></a>
				</div>
			<?php endif; ?>

			<?php if (get_field('email','options')): ?>
				<div class="item">
					<i class="fas fa-envelope"></i> <a href="mailto:<?= antispambot(get_field('email', 'option'));?>"><?= antispambot(get_field('email', 'option'));?></a>
				</div>
			<?php endif; ?>

			<div class="item social">
				<?php if (get_field('linkedin', 'options')): ?>
					<a href="<?=get_field('linkedin', 'options');?>" target="_blank"><i class="fab fa-linkedin-in"></i></a>
				<?php endif; ?>

				<?php if (get_field('facebook', 'options')): ?>
					<a href="<?=get_field('facebook', 'options');?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
				<?php endif; ?>

				<?php if (get_field('twitter', 'options')): ?>
					<a href="<?=get_field('twitter', 'options');?>" target="_blank"><i class="fab fa-twitter"></i></a>
				<?php endif; ?>

				<?php if (get_field('googleplus', 'options')): ?>
					<a href="<?=get_field('googleplus', 'options');?>" target="_blank"><i class="fab fa-google-plus-g"></i></a>
				<?php endif; ?>

				<?php if (get_field('instagram', 'options')): ?>
					<a href="<?=get_field('instagram', 'options');?>" target="_blank"><i class="fab fa-instagram"></i></a>
				<?php endif; ?>

				<?php if (get_field('youtube', 'options')): ?>
					<a href="<?=get_field('youtube', 'options');?>" target="_blank"><i class="fab fa-youtube"></i></a>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<?php get_template_part('template-parts/nav', 'desktop'); ?>
	<?php get_template_part('template-parts/nav', 'mobile'); ?>
